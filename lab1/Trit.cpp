#include "Trit.h"

Trit::Trit(){
    value = Unknown;
}
Trit::Trit(trit inValue){
    value = inValue;
};

Trit::operator trit() const{
    return value;
};
const Trit Trit::operator &(const Trit& rTrit) const{
    switch (value) {
        case False: switch (rTrit.value) {
                case False: return False;
                case True: return False;
                case Unknown: return False;

        }
        case Unknown: switch (rTrit.value) {
                case False: return False;
                case True: return Unknown;
                case Unknown: return Unknown;

            }
        case True:switch (rTrit.value) {
                case False: return False;
                case True: return True;
                case Unknown: return Unknown;

            }
        default: return Unknown;
    }
};
const Trit Trit::operator &(const trit& rtrit) const{
  Trit T = rtrit;
  return *this & T;
};
const Trit operator &(const trit& ltrit, const Trit& rTrit) {
    Trit T = ltrit;
    return T & rTrit;

};


const Trit Trit::operator |(const Trit& rTrit) const{
    switch (value) {
        case False: switch (rTrit.value) {
                case False: return False;
                case True: return True;
                case Unknown: return Unknown;

            }
        case Unknown: switch (rTrit.value) {
                case False: return Unknown;
                case True: return True;
                case Unknown: return Unknown;

            }
        case True:switch (rTrit.value) {
                case False: return True;
                case True: return True;
                case Unknown: return True;

            }
        default: return Unknown;
    }
};
const Trit Trit::operator |(const trit& rtrit) const{
    Trit T = rtrit;
    return *this | T;
};

const Trit operator |(const trit& ltrit, const Trit& rTrit){
    Trit T = ltrit;
    return T | rTrit;
};


 bool Trit::operator ==(const Trit& rTrit) const{
    if(value == rTrit.value)
        return true;
    else
        return false;
};
 bool Trit::operator ==(const trit& rtrit) const{
    Trit T = rtrit;
    return *this == T;
};
 bool operator ==(const trit& ltrit, const Trit& rTrit){
     Trit T = ltrit;
     return T == rTrit;
};


const Trit Trit::operator !() const{
    switch (value) {
        case False: return True;
        case True: return False;
        case Unknown: return Unknown;
        default: return Unknown;
    }
};


 std::ostream& operator<<(std::ostream& stream, Trit trit){
     switch (trit.value) {
         case False:
         {stream << "False"; return stream;}
         case True:
         {stream << "True";return stream;}
         case Unknown:
         {stream << "Unknown";return stream;}
     }



};
