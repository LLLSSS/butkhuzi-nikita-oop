#include <iostream>
#include "TritSet.h"
#include <gtest/gtest.h>

int main (int argc, char* argv[]){
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

TEST(TritSet,capacity){
TritSet set(20);
size_t allocLenght = set.capacity();
ASSERT_TRUE(allocLenght == 20);

set[300] = True;
allocLenght = set.capacity();
ASSERT_TRUE(allocLenght == 300);

set[500] = Unknown;
size_t allocLenghtNew = set.capacity();
ASSERT_TRUE(allocLenght == allocLenghtNew);
}

TEST(TritSet, shrink){
TritSet set(100);
size_t allocLenght = set.lengthi();
set[50] = True;
ASSERT_TRUE(allocLenght == set.lengthi());
set.shrink();
ASSERT_TRUE(allocLenght == set.lengthi());

set[150] = True;
allocLenght = set.lengthi();
set[150] = Unknown;
set.shrink();
ASSERT_TRUE(allocLenght > set.lengthi());
ASSERT_TRUE(set.lengthi() == (100*2 - 1)/(8*sizeof(uint32_t)) + 1);
}

TEST(TritSet, length){
TritSet set(100);
set[50] = True;
ASSERT_TRUE(set.length() == 51);
set[50] = Unknown;
ASSERT_TRUE(set.length() == 1);
}

TEST(TritSet, trim){
TritSet set(100);
set[50] = set[51] = True;
set.trim(51);
ASSERT_TRUE(set[50] == True);
ASSERT_TRUE(set[51] == True);
}

TEST(TritSet, cardinality){
TritSet set(100);
set[1] = set[2] = set[3] = True;
set[4] = set[5] = set[6] = set [9] = False;
ASSERT_TRUE(set.cardinality(True) == 3);
ASSERT_TRUE(set.cardinality(False) == 4);
ASSERT_EQ(set.cardinality(Unknown), 3);
}

TEST(TritSet, operators){
TritSet set(100);
set[0] = False;
set[1] = Unknown;
set[2] = True;
ASSERT_TRUE((set[0]&set[0]) == False); // &
ASSERT_TRUE((set[0]&set[1]) == False);
ASSERT_TRUE((set[1]&set[0]) == False);
ASSERT_TRUE((set[1]&set[2]) == Unknown);
ASSERT_TRUE((set[2]&set[1]) == Unknown);
ASSERT_TRUE((set[1]&set[1]) == Unknown);
ASSERT_TRUE((set[2]&set[2]) == True);
ASSERT_TRUE((set[2]&set[0]) == False);
ASSERT_TRUE((set[0]&set[2]) == False);

ASSERT_TRUE((set[0]|set[0]) == False); // |
ASSERT_TRUE((set[0]|set[1]) == Unknown);
ASSERT_TRUE((set[1]|set[0]) == Unknown);
ASSERT_TRUE((set[1]|set[2]) == True);
ASSERT_TRUE((set[2]|set[1]) == True);
ASSERT_TRUE((set[1]|set[1]) == Unknown);
ASSERT_TRUE((set[2]|set[2]) == True);
ASSERT_TRUE((set[2]|set[0]) == True);
ASSERT_TRUE((set[0]|set[2]) == True);

TritSet setA(100);  // &=
setA[5] = False;
setA[6] = True;
setA[7] = Unknown;
TritSet setB(200);
setB[5] = True;
setB[6] = True;
setB[7] = True;
setB[150] = True;
setB[151] = False;
setB[152] = Unknown;
setA &= setB;
ASSERT_TRUE(setA.capacity() == setB.capacity());
ASSERT_TRUE(setA[5] == False);
ASSERT_TRUE(setA[6] == True);
ASSERT_TRUE(setA[7] == Unknown);
ASSERT_TRUE(setA[150] == Unknown);
ASSERT_TRUE(setA[151] == False);
ASSERT_TRUE(setA[152] == Unknown);

ASSERT_FALSE(setA[300] == True);

TritSet setA1(100);  // |=
setA1[5] = False;
setA1[6] = True;
setA1[7] = Unknown;
TritSet setB1(200);
setB1[5] = True;
setB1[6] = True;
setB1[7] = True;
setB1[150] = True;
setB1[151] = False;
setB1[152] = Unknown;
setA1 |= setB1;
ASSERT_TRUE(setA1.capacity() == setB1.capacity());
ASSERT_TRUE(setA1[5] == True);
ASSERT_TRUE(setA1[6] == True);
ASSERT_TRUE(setA1[7] == True);
ASSERT_TRUE(setA1[150] == True);
ASSERT_TRUE(setA1[151] == Unknown);
ASSERT_TRUE(setA1[102] == Unknown);

TritSet setC(100);  // !
setC[0] = True;
setC[1] = Unknown;
setC[2] = False;
TritSet setNoSetC = !setC;
ASSERT_TRUE(setNoSetC[0] == False);
ASSERT_TRUE(setNoSetC[1] == Unknown);
ASSERT_TRUE(setNoSetC[2] == True);

}