package MathCommand;

import Calculator.Command.CommandExecuteException;
import Calculator.Main.Context;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SqrtTest {

    @Test
    void SqrtTests() throws CommandExecuteException {
        Context context = new Context();
        Sqrt sqrt = new Sqrt();
        context.pushToStack(36.0);
        sqrt.execute(context, null);
        assertEquals (context.peekAtStack(), 6.0);
    }
}