package MathCommand;

import Calculator.Command.CommandExecuteException;
import Calculator.Main.Context;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SummTest {

    @Test
    void SummTests() throws CommandExecuteException {
        Context context = new Context();
        Summ summ = new Summ();
        context.pushToStack(10.0);
        context.pushToStack(2.0);
        summ.execute(context, null);
        assertEquals (context.peekAtStack(), 12.0);
    }
}