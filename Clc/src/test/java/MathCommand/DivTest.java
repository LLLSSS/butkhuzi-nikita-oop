package MathCommand;

import Calculator.Command.CommandExecuteException;
import Calculator.Main.Context;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DivTest {

    @Test
    void DivTests() throws CommandExecuteException {
        Context context = new Context();
        Div div = new Div();
        context.pushToStack(10.0);
        context.pushToStack(2.0);
        div.execute(context, null);
        assertEquals (context.peekAtStack(), 5);
    }
}