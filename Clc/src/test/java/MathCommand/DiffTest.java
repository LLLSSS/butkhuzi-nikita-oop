package MathCommand;

import Calculator.Command.CommandExecuteException;
import Calculator.Main.Context;

import static org.junit.jupiter.api.Assertions.*;

class DiffTest {
    @org.junit.jupiter.api.Test
    void DiffTests() throws CommandExecuteException {
        Context context = new Context();
        Diff diff = new Diff();
        context.pushToStack(5.0);
        context.pushToStack(7.0);
        diff.execute(context, null);
        assertEquals (context.peekAtStack(), 2);
    }
}