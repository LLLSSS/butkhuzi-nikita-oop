package MathCommand;

import Calculator.Command.CommandExecuteException;
import Calculator.Main.Context;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MulTest {

    @Test
    void MulTests() throws CommandExecuteException {
        Context context = new Context();
        Mul mul = new Mul();
        context.pushToStack(10.0);
        context.pushToStack(2.0);
        mul.execute(context, null);
        assertEquals (context.peekAtStack(), 20.0);
    }
}