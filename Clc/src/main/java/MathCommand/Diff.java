package MathCommand;
import Calculator.Command.CalcCommand;
import Calculator.Command.CommandExecuteException;
import Calculator.Command.Push;
import Calculator.Main.Context;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import java.util.EmptyStackException;
import java.util.List;

public class Diff implements CalcCommand {
    @Override
    public void execute(Context context, List<String> myArgs) throws CommandExecuteException
    {
        try {
            double b = context.popFromStack();
            double a = context.popFromStack();

            context.pushToStack(b - a);
        } catch (EmptyStackException e) {
            throw new CommandExecuteException("Cannot execute command because stack is empty ", e);
        }
    }
}
