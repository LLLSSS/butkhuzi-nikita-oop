package Calculator.Main;
import Calculator.Command.CalcCommand;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.TreeMap;

import static Calculator.Main.Calculator.logger;

public class CmdFactory {
    private static Properties properties = null;
    private static TreeMap<String, Class> classes = null;

    static {
        classes = new TreeMap<String, Class>();
        InputStream conformancesAsStream = null;

        try {
            conformancesAsStream = new FileInputStream("CmdFactory.properties");
            properties = new Properties();
            properties.load(conformancesAsStream);
        } catch (IOException e) {
            System.out.println("IOException occurred in CommandsFactory.properties " + e.getLocalizedMessage());
        } finally {
            try {
                if (null != conformancesAsStream) {
                    conformancesAsStream.close();
                }
            } catch (IOException e) {
                System.out.println("IOException: " + e.getLocalizedMessage());
            }
        }
    }

    public static CalcCommand getCommand(String cmdName) throws ClassNotFoundException, IllegalAccessException, InstantiationException
    {
        CalcCommand command;
        Class<?> cmdClass;

        String commandUp = cmdName.toUpperCase();

        String key = properties.getProperty(commandUp);
        if (null == key) {
            logger.warn("Try not exist Command");
            throw new NoSuchElementException("Invalid command request: " + commandUp);
        }
        if (!classes.containsKey(key)) {
            cmdClass = Class.forName(key);
            classes.put(cmdName, cmdClass);
            logger.info("There is a command " + commandUp);
        } else {
            cmdClass = classes.get(cmdName);
            logger.info("There is a" + commandUp);
        }
        try{
            command = (CalcCommand)cmdClass.getDeclaredConstructor().newInstance();
            return command;
        }
        catch (NoSuchMethodException e ) {System.out.println("NoSuchMethodException: " + e.getLocalizedMessage()); System.exit(0);}
        catch (InvocationTargetException e) {System.out .println("InvocationTargetException: " + e.getLocalizedMessage()); System.exit(0);}
        return null;
    }
}
