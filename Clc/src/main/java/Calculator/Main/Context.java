package Calculator.Main;

import java.util.EmptyStackException;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.TreeMap;

public class Context {
    private final LinkedList<Double> calcStack = new LinkedList<Double>();
    private final TreeMap<String, Double> constants = new TreeMap<String, Double>();

    public double getConstant(String var) throws NoSuchElementException
    {
        if (!constants.containsKey(var)) {
            throw new NoSuchElementException("Stack doesn't contain Constant " + var);
        }
        return constants.get(var);
    }

    public void setConstant(String var, Double value)
    {
        constants.put(var, value);
    }

    public void pushToStack(double value)
    {
        calcStack.push(value);
    }

    public double popFromStack() throws EmptyStackException
    {
        return calcStack.pop();
    }

    public double peekAtStack() throws EmptyStackException
    {   assert calcStack.peek() != null;
        return calcStack.peek();
    }
}
