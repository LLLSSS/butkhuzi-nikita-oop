package Calculator.Main;
import Calculator.Command.CalcCommand;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import Calculator.Command.Push;
import org.apache.log4j.PropertyConfigurator;;
import org.apache.log4j.Logger;


public class Calculator {
    private final static Context context = new Context();
    public static Logger logger = Logger.getLogger(Calculator.class);
    public static void main(String[] args) {
            String log4jConfigFile = System.getProperty("user.dir")
                    + File.separator + "log4j.properties";
            PropertyConfigurator.configure(log4jConfigFile);
        printCommands();
        Scanner scanner = getScanner(args);
        assert (null != scanner);


        while (scanner.hasNextLine()) {
            String commandStr = scanner.nextLine();
            commandStr = clearString(commandStr);
            if (isInsignificantCommand(commandStr)) {
                continue;
            }

            executeCommand(commandStr);

        }
        scanner.close();

    }

    private static void executeCommand(String commandStr) {
        String[] cmd = commandStr.split(" ");
        List<String> cmdArguments = createCmdArguments(cmd);

        try {
            CalcCommand command = CmdFactory.getCommand(cmd[0]);
            assert command != null;
            command.execute(context, cmdArguments);
        } catch (Throwable e) {
            System.err.println("Error execute command: " + e.getLocalizedMessage());

        }
    }

    private static List<String> createCmdArguments(String[] cmd) {
        assert (null != cmd);
        return new ArrayList<String>(Arrays.asList(cmd).subList(1, cmd.length));
    }

    private static boolean isInsignificantCommand(String command) {
        return command.equals("") || command.startsWith("#");
    }

    private static Scanner getScanner(String[] args) {
        assert (null != args);
        Scanner scanner = null;
        if (1 == args.length) {
            String fileName = args[0];

            try {
                FileInputStream fileInputStream = new FileInputStream(fileName);
                BufferedInputStream inputStream = new BufferedInputStream(fileInputStream);
                scanner = new Scanner(inputStream);
            } catch (FileNotFoundException e) {
                System.err.println(e.getMessage());
                System.exit(0);
            }

        } else {
            scanner = new Scanner(System.in);
        }

        return scanner;
    }

    private static String clearString(String str) {
        return str.trim().replaceAll("\\s+", " ");
    }

    private static void printCommands()
    {
        String str = "COMMANDS REFERENCE\n" +
                "1) POP, PUSH;\n" +
                "2) + , - , * , /, SQRT;\n" +
                "3) PRINT;\n" +
                "4) DEFINE parameterName parameterValue;\n";
        System.out.println(str);
    }


}
