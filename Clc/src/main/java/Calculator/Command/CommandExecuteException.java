package Calculator.Command;

public class CommandExecuteException extends Exception {
    public CommandExecuteException(String message)
    {
        super(message);
    }

    public CommandExecuteException(String message, Throwable cause)
    {
        super(message, cause);
    }


}
