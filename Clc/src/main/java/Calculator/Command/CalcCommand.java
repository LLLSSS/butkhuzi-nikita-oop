package Calculator.Command;
import Calculator.Main.Context;

import java.util.List;

public interface CalcCommand {
    public void execute(Context context , List<String> myArgs ) throws CommandExecuteException;

}
