package Calculator.Command;
import Calculator.Main.Context;
import java.util.EmptyStackException;
import java.util.List;

import static Calculator.Main.Calculator.logger;

public class Pop implements CalcCommand {

    @Override
    public void execute(Context context, List<String> myArgs ) throws CommandExecuteException {
        try {
            context.popFromStack();
        } catch(EmptyStackException e) {
            logger.warn("Try pop empty stack");
            throw new CommandExecuteException("Cannot execute command because stack is empty", e);
        }
    }
}
