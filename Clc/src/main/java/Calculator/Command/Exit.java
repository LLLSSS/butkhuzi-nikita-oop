package Calculator.Command;
import Calculator.Main.Context;
import java.util.EmptyStackException;
import java.util.List;

public class Exit implements CalcCommand {
    @Override
    public void execute(Context context, List<String> myArgs) throws CommandExecuteException{
        try {
            System.exit(0);
        }
        catch (EmptyStackException e) {
            throw new CommandExecuteException("Cannot exit", e);
        }
    }
    }

