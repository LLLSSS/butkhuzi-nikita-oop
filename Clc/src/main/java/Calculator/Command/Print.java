package Calculator.Command;
import Calculator.Main.Context;
import java.util.EmptyStackException;
import java.util.List;

import static Calculator.Main.Calculator.logger;

public class Print implements CalcCommand {
    @Override
    public void execute(Context context, List<String> myArgs) throws CommandExecuteException
    {
        try {
            double a = context.peekAtStack();
            System.out.println("Result: " + a);
        } catch (EmptyStackException e) {
            logger.warn("Trying print empty stack");
            throw new CommandExecuteException("Cannot execute command because stack is empty", e);
        }
    }
}
