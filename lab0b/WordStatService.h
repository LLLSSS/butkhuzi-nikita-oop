#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>

using namespace std;

class WordStatService {
private:
    map <string, int> dictionary;
    int wordCount;
    vector< pair<string, pair<int, double>> > table;
    void getCountWord();
    float getFrequency(int const num);
    void setTable();

public:
    WordStatService();
    void insert(vector<string> words);
    vector<pair<string, pair<int, double>>>  getTable();

};