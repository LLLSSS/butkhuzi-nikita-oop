#include "ReportService.h"

ReportService::ReportService(string newPath) {
path = newPath;
file.open(path);
if(!file.is_open())
status = false;
else
status = true;
};

void ReportService::printTab(vector< pair<string, pair<int, double>>> table){
int size = table.size();
for(int i = 0; i < size; i++)
file <<  table[i].first << ";" << table[i].second.first << ";" << table[i].second.second << '\n';
};

bool ReportService::isOpen() { return status; };

ReportService::~ReportService() { file.close(); };