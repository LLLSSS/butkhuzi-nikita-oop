#include "WordStatService.h"

void WordStatService::getCountWord() {
    wordCount = 0;
    map <string, int> :: iterator it = dictionary.begin();
    for(int i = 0; it != dictionary.end(); i++, it++)
        wordCount += it->second;
};

float WordStatService::getFrequency(int const num) { return ((float)num)/((float)wordCount)*100.0; };

void WordStatService::setTable(){
    table.clear();
    map <string, int> :: iterator it = dictionary.begin();

    for(int i = 0; it != dictionary.end(); i++, it++) {
        table.push_back(make_pair(it->first, make_pair(it->second, getFrequency(it->second) ) ) );
    }

    sort(table.begin(), table.end(), [](auto &left, auto &right) -> bool {
        return left.second.first > right.second.first;
    });
};
WordStatService::WordStatService(){wordCount = 0;};

void WordStatService::insert(vector<string> words){
    map <string, int> :: iterator itr = dictionary.begin();
    for(int i = 0; i < (int)words.size(); i++) {
        itr = dictionary.find(words[i]);
        if(dictionary.end() == itr)
            dictionary.insert(pair<string, int>(words[i], 1));
        else
            dictionary.insert(pair<string, int>(words[i], ++itr->second));
    }
};

vector<pair<string, pair<int, double>>>  WordStatService::getTable() {
    getCountWord();
    setTable();
    return table;
};
