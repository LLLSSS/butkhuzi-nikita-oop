#include <iostream>
#include <fstream>
#include <string>
using namespace std;

class FileRecoder {
private:
    string line;
    string path;
    ifstream file;
    bool status;
public:
    FileRecoder(string newPath);

    bool nextLine();

    string gettingLine();

    bool isOpen();

    ~FileRecoder();
};