#include <string>
#include <vector>
#include <gtest/gtest.h>
#include "FileRecorder.h"
#include "TokenService.h"
#include "WordStatService.h"
#include "ReportService.h"

using namespace std;


int main(int arg, char* argv[]) {
    if(arg != 3) {
        cout << "Incorrect number of arguments";
        return 0;
    }

    FileRecoder FileRec(argv[1]);
    ReportService FileOut(argv[2]);
    if(!FileRec.isOpen())
        cout << "Cannot open the file.\n";
    else {
        TokenService wordList;

        WordStatService dict;

        while (FileRec.nextLine())
            dict.insert(  wordList.splitLine( FileRec.gettingLine() ) );
        if(!FileOut.isOpen())
            cout << "Cannot open the file.\n";
        else
            FileOut.printTab(dict.getTable());
    }

    return 0;
}



/*
int main(int argc, char* argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

TEST(FileRecorderTests, ConstructorTests) {
    FileRecoder testRec1("C:\\Progr\\Test.txt");
    ASSERT_TRUE(testRec1.isOpen());
    FileRecoder  testRec2("none");
    ASSERT_TRUE(!testRec2.isOpen());
}
TEST(FileRecorderTests, NextLineTests) {
    FileRecoder testRec1("C:\\Progr\\Test.txt");
    ASSERT_TRUE(testRec1.nextLine());
    ASSERT_TRUE(!testRec1.nextLine());
}
TEST(FileRecoderTests, GettingLineTests){
    FileRecoder testRec1("C:\\Progr\\Test.txt");
    testRec1.nextLine();
    ASSERT_EQ(testRec1.gettingLine(), "First line.");
    testRec1.nextLine();
    ASSERT_EQ(testRec1.gettingLine(), "");
}
TEST(TokenServiceTests, SplitLineTests){
    TokenService testTS;
    ASSERT_TRUE(testTS.splitLine("\n\\.,^*:;()!?\"\'\t").empty());
    ASSERT_TRUE(testTS.splitLine("").empty());

    vector <string> words = testTS.splitLine("AAaaAAAaaAAaa.  AAAAaaaaAAAaaaa!");
    ASSERT_TRUE( 2 == words.size());
    ASSERT_EQ(words[0], "aaaaaaaaaaaaa");
    ASSERT_EQ(words[1], "aaaaaaaaaaaaaaa");
}

TEST(WordStatServiceTests, InsertWordsTests) {
    vector<string> testVect1;
    testVect1.push_back("dog");
    testVect1.push_back("cat");
    testVect1.push_back("horse");
    testVect1.push_back("bird");
    testVect1.push_back("frog");
    testVect1.push_back("bear");
    WordStatService testDict1;
    testDict1.insert(testVect1); testVect1.clear();


    ASSERT_TRUE(testDict1.getTable().size() == 6);
    testVect1.push_back("fox");
    testVect1.push_back("asd");
    testVect1.push_back("dog");
    testVect1.push_back("dog");
    testVect1.push_back("ew");
    testVect1.push_back("qwe");
    testVect1.push_back("ew");
    testDict1.insert(testVect1); testVect1.clear();

    ASSERT_TRUE(testDict1.getTable().size() == 10);

    ASSERT_TRUE(testDict1.getTable()[0].second.first == 3);
}
*/