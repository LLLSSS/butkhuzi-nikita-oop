#include "TokenService.h"

bool TokenService::ifSymbol(char symb) {
    if((symb >= 'a' && symb <= 'z') ||(symb >= '0' && symb <= '9'))
        return true;
    else
        return false;
};
vector<string> TokenService::splitLine(string str){
    vector<string> wordList;
    string word = "\0";
    for(int i = 0; i <= (int)str.size(); i++) {
        char symbol = (char)tolower((char)str[i]);
        if(ifSymbol(symbol))
            word += symbol;
        else {
            if(word[0] != '\0') {
                word += "\0";
                wordList.push_back(word);
            }
            word = "\0";
        }
    }
    return wordList;
};