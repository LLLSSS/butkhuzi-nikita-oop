package Fabrica.Parts;

public class Car {
    public final Body body;
    public final Motor motor;
    public final Accessories accessories;
    public final int cID;
    public Car(int cID, Body body, Motor motor, Accessories accessories) {
        this.cID = cID;
        this.body = body;
        this.motor = motor;
        this.accessories = accessories;
    }
}
