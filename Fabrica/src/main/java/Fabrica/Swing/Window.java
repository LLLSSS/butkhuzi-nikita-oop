package Fabrica.Swing;
import Fabrica.Contorl.VisualOperator;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.Duration;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class Window extends JFrame implements ActionListener {
    private Duration BODY_DEALER_TIME;
    private Duration MOTOR_DEALER_TIME;


    JLabel motor = new JLabel("Motor");
    JLabel body = new JLabel("Body");
    JLabel accessories = new JLabel("Accessories");
    JLabel mainStorage = new JLabel("Car storage");



    public void setFullnessBodyStorage(int size, int capacity) {

        String output = String.format("Body %d/%d", size, capacity);
        body.setText(output);
    }


    public void setFullnessMotorStorage(int size, int capacity) {

        String output = String.format("Motor %d/%d", size, capacity);
        motor.setText(output);
    }


    public void setFullnessAccessoriesStorage(int size, int capacity) {

        String output = String.format("Accessories %d/%d", size, capacity);
        accessories.setText(output);
    }


    public void setFullnessCarStorage(int size, int capacity) {

        String output = String.format("Car storage %d/%d", size, capacity);
        mainStorage.setText(output);
    }

    private Duration ACCESSORIES_DEALER_TIME;
    private Duration CAR_DEALER_TIME;
    private Duration WORKER_TIME;
    private JLabel label;
    public Window(){
        super("Factory");

        BODY_DEALER_TIME = Duration.ofSeconds(VisualOperator.getBODY_DEALER_TIME_DEFAULT());
        MOTOR_DEALER_TIME = Duration.ofSeconds(VisualOperator.getMOTOR_DEALER_TIME_DEFAULT());
        ACCESSORIES_DEALER_TIME = Duration.ofSeconds(VisualOperator.getACCESSORIES_DEALER_TIME_DEFAULT());
        CAR_DEALER_TIME = Duration.ofSeconds(VisualOperator.getCAR_DEALER_TIME_DEFAULT());
        WORKER_TIME = Duration.ofSeconds(VisualOperator.getWORKER_TIME_DEFAULT());

        setDefaultCloseOperation(EXIT_ON_CLOSE);

        JSlider slider1 = new JSlider(0, 10);
        slider1.setPaintTicks(true);
        slider1.setPaintLabels(true);
        slider1.setMajorTickSpacing(1);

        JSlider slider2 = new JSlider(0, 10);
        slider2.setPaintTicks(true);
        slider2.setPaintLabels(true);
        slider2.setMajorTickSpacing(1);

        JSlider slider3 = new JSlider(0, 10);
        slider3.setPaintTicks(true);
        slider3.setPaintLabels(true);
        slider3.setMajorTickSpacing(1);

        JSlider slider4 = new JSlider(0, 15);
        slider4.setPaintTicks(true);
        slider4.setPaintLabels(true);
        slider4.setMajorTickSpacing(3);

        JSlider slider5 = new JSlider(0, 15);
        slider5.setPaintTicks(true);
        slider5.setPaintLabels(true);
        slider5.setMajorTickSpacing(3);

        slider1.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                int value = ((JSlider)e.getSource()).getValue();
                MOTOR_DEALER_TIME = Duration.ofSeconds(value);
            }
        });
        slider2.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                int value = ((JSlider)e.getSource()).getValue();
                BODY_DEALER_TIME = Duration.ofSeconds(value);
            }
        });
        slider3.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                int value = ((JSlider)e.getSource()).getValue();
                ACCESSORIES_DEALER_TIME = Duration.ofSeconds(value);
            }
        });
        slider4.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                int value = ((JSlider)e.getSource()).getValue();
                WORKER_TIME = Duration.ofSeconds(value);
            }
        });
        slider5.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                int value = ((JSlider)e.getSource()).getValue();
                CAR_DEALER_TIME = Duration.ofSeconds(value);
            }
        });

        Font font = new Font("Arial", Font.PLAIN, 14);

        motor.setFont(font);
        body.setFont(font);
        accessories.setFont(font);
        JLabel workers = new JLabel("Workers");
        workers.setFont(font);
        JLabel dealers = new JLabel("Dealers");
        dealers.setFont(font);

        JPanel contents = new JPanel(new VerticalLayout());


        contents.add(motor);
        contents.add(slider1);

        contents.add(body);
        contents.add(slider2);

        contents.add(accessories);
        contents.add(slider3);

        contents.add(workers);
        contents.add(slider4);

        contents.add(dealers);
        contents.add(slider5);

        mainStorage.setFont(font);
        contents.add(mainStorage);

        getContentPane().add(contents);

        setSize(250, 450);
        setVisible(true);

    }


    public void actionPerformed(ActionEvent e) {

    }


    public Duration getBODY_DEALER_TIME() {
        return BODY_DEALER_TIME;
    }


    public Duration getMOTOR_DEALER_TIME() {
        return MOTOR_DEALER_TIME;
    }


    public Duration getACCESSORIES_DEALER_TIME() {
        return ACCESSORIES_DEALER_TIME;
    }


    public Duration getCAR_DEALER_TIME() {
        return CAR_DEALER_TIME;
    }


    public Duration getWORKER_TIME() {
        return WORKER_TIME;
    }


}

class   VerticalLayout implements LayoutManager
{
    private Dimension size = new Dimension();

    public void addLayoutComponent   (String name, Component comp) {}
    public void removeLayoutComponent(Component comp) {}

    public Dimension minimumLayoutSize(Container c) {
        return calculateBestSize(c);
    }
    public Dimension preferredLayoutSize(Container c) {
        return calculateBestSize(c);
    }
    public void layoutContainer(Container container)
    {

        Component list[] = container.getComponents();
        int currentY = 5;
        for (int i = 0; i < list.length; i++) {

            Dimension pref = list[i].getPreferredSize();
            list[i].setBounds(5, currentY, pref.width, pref.height);
            currentY += 5;
            currentY += pref.height;
        }
    }
    private Dimension calculateBestSize(Container c)
    {

        Component[] list = c.getComponents();
        int maxWidth = 0;
        for (int i = 0; i < list.length; i++) {
            int width = list[i].getWidth();
            if ( width > maxWidth )
                maxWidth = width;
        }
        size.width = maxWidth + 5;

        int height = 0;
        for (int i = 0; i < list.length; i++) {
            height += 5;
            height += list[i].getHeight();
        }
        size.height = height;
        return size;
    }
}