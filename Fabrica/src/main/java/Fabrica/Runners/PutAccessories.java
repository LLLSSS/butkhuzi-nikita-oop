package Fabrica.Runners;
import Fabrica.Factory;
import Fabrica.Storage;
import Fabrica.Parts.Accessories;

import java.util.concurrent.TimeUnit;

public class PutAccessories implements Runnable {
    Storage<Accessories> accessoriesStorage;
    public PutAccessories(Storage<Accessories> accessoriesStorage){
        this.accessoriesStorage = accessoriesStorage;
    }
    @Override
    public void run() {
        while (true){
            try {
                TimeUnit.SECONDS.sleep(Factory.timeSettings.ACCESSORIES_DEALER_TIME().getSeconds());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            accessoriesStorage.put(new Accessories((int) (Math.random() * 1000)));
        }
    }
}
