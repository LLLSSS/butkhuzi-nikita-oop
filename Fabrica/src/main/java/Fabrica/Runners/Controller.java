package Fabrica.Runners;
import Fabrica.Storage;
import Fabrica.Parts.Car;

public class Controller implements Runnable {

    private final Storage<Car> carStorage;
    private final Checker checker;

    public Controller(Storage<Car> carStorage, Checker checker) {
        this.carStorage = carStorage;
        this.checker = checker;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (carStorage){
                while(carStorage.getSize() >= 100){
                    try {
                        carStorage.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                synchronized (checker){ checker.notify(); }
            }
        }
    }
    static public class Checker{};


}