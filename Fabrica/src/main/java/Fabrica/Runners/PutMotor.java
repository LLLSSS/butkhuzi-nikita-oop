package Fabrica.Runners;
import Fabrica.Factory;
import Fabrica.Storage;
import Fabrica.Parts.Motor;
import java.util.concurrent.TimeUnit;

public class PutMotor implements Runnable {
    private final Storage<Motor> motorStorage;

    public PutMotor(Storage<Motor> motorStorage) {
        this.motorStorage = motorStorage;
    }

    @Override
    public void run() {
        while (true) {
            try {
                TimeUnit.SECONDS.sleep(Factory.timeSettings.MOTOR_DEALER_TIME().getSeconds());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            motorStorage.put(new Motor((int) (Math.random() * 1000)));
        }
    }
}
