package Fabrica.Runners;
import Fabrica.Factory;
import Fabrica.Storage;
import Fabrica.Parts.Accessories;
import Fabrica.Parts.Body;
import Fabrica.Parts.Car;
import Fabrica.Parts.Motor;
import java.util.concurrent.TimeUnit;

public class Worker implements Runnable {
    private final Storage<Accessories> accessoriesStorage;
    private final Storage<Body> bodyStorage;
    private final Storage<Motor> motorStorage;
    private final Storage<Car> carStorage;
    private final Controller.Checker checker;


    public Worker(Storage<Accessories> accessoriesStorage, Storage<Body> bodyStorage,
                  Storage<Motor> motorStorage, Storage<Car> carStorage, Controller.Checker checker) {
        this.bodyStorage = bodyStorage;
        this.carStorage = carStorage;
        this.motorStorage = motorStorage;
        this.accessoriesStorage = accessoriesStorage;
        this.checker = checker;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (checker){
                try {
                    checker.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            Body body = bodyStorage.get();
            Motor motor = motorStorage.get();
            Accessories accessories = accessoriesStorage.get();
            try {
                TimeUnit.SECONDS.sleep(Factory.timeSettings.WORKER_TIME().getSeconds());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Car car = new Car((int) (Math.random()*1000), body, motor, accessories);
            carStorage.put(car);
        }
    }
}
