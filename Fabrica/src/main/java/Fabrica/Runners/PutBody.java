package Fabrica.Runners;
import Fabrica.Factory;
import Fabrica.Storage;
import Fabrica.Parts.Body;

import java.util.concurrent.TimeUnit;

public class PutBody implements Runnable {
    private final Storage<Body> bodyStorage;

    public PutBody(Storage<Body> bodyStorage) {
        this.bodyStorage = bodyStorage;
    }

    @Override
    public void run() {
        while (true) {
            try {
                TimeUnit.SECONDS.sleep(Factory.timeSettings.BODY_DEALER_TIME().getSeconds());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            bodyStorage.put(new Body((int) (Math.random() * 1000)));
        }
    }
}