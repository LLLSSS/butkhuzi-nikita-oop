package Fabrica.Runners;
import Fabrica.Factory;
import Fabrica.Storage;
import Fabrica.Parts.Car;

import java.util.concurrent.TimeUnit;

import static Fabrica.Factory.logger;

public class GetCar implements Runnable {
    private static Integer AlldID = 1;
    private Integer dID;
    private final Storage<Car> carStorage;

    public GetCar(Storage<Car> carStorage) {
        this.carStorage = carStorage;
        dID = AlldID;
        AlldID++;
    }
    @Override
    public void run() {
        while (true) {
            try {
                TimeUnit.SECONDS.sleep(Factory.timeSettings.CAR_DEALER_TIME().getSeconds());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (carStorage) {
                Car car = carStorage.get();
                logger.info("Get: Dealer "+ this.dID + ": Auto "+ car.cID +
                        " (Body: "+ car.body.bID +" Motor: "+ car.motor.eID + " Accessory: "+ car.accessories.aID + ")");

                carStorage.notify();
            }
        }
    }

}
