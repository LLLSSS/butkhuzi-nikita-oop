package Fabrica;

import Fabrica.Contorl.VisualOperator;
import org.apache.log4j.PropertyConfigurator;;
import org.apache.log4j.Logger;
import Fabrica.Contorl.TimeSettings;
import Fabrica.Parts.Accessories;
import Fabrica.Parts.Body;
import Fabrica.Parts.Car;
import Fabrica.Parts.Motor;
import Fabrica.Runners.*;
import Fabrica.Swing.Window;
import ThreadPool.ThreadPool;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import static java.lang.Integer.parseInt;

public class Factory {
    private Storage<Accessories> accessoriesStorage;
    private Storage<Body> bodyStorage;
    private Storage<Motor> motorStorage;
    private Storage<Car> carStorage;

    public static TimeSettings timeSettings = new TimeSettings();

    public static Logger logger = Logger.getLogger(Factory.class);

    public void main(String[] args) throws InterruptedException {
        Window Win = new Window();
        String log4jConfigFile = System.getProperty("user.dir")
                + File.separator + "log4j.properties";
        PropertyConfigurator.configure(log4jConfigFile);

        FileInputStream File = null;
        try {
            File = new FileInputStream("FactoryConfig.properties");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Properties property = new Properties();
        try {
            property.load(File);
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        ThreadPool accessoriesThreadPool = new ThreadPool(parseInt(property.getProperty("AccessorySuppliers")));
        ThreadPool bodyThreadPool = new ThreadPool(1);
        ThreadPool motorThreadPool = new ThreadPool(1);
        ThreadPool workersThreadPool = new ThreadPool(parseInt(property.getProperty("Workers")));
        ThreadPool dealersThreadPool = new ThreadPool(parseInt(property.getProperty("Dealers")));
        ThreadPool controllersThreadPool = new ThreadPool(1);

        accessoriesStorage = new Storage<>(parseInt(property.getProperty("StorageAccessorySize")));
        bodyStorage = new Storage<>(parseInt(property.getProperty("StorageBodySize")));
        motorStorage = new Storage<>(parseInt(property.getProperty("StorageMotorSize")));
        carStorage = new Storage<>(parseInt(property.getProperty("StorageAutoSize")));

        Controller.Checker checker = new Controller.Checker();

        VisualOperator.start(Win, timeSettings, motorStorage, bodyStorage, carStorage, accessoriesStorage);
        for (int i = 0; i < parseInt(property.getProperty("AccessorySuppliers")); i++) {
            accessoriesThreadPool.execute(new PutAccessories(accessoriesStorage));
        }
        bodyThreadPool.execute(new PutBody(bodyStorage));
        motorThreadPool.execute(new PutMotor(motorStorage));
        for (int i = 0; i < parseInt(property.getProperty("Workers")); i++) {
            workersThreadPool.execute(new Worker(accessoriesStorage, bodyStorage, motorStorage, carStorage, checker));
        }
        for (int i = 0; i < parseInt(property.getProperty("Dealers")); i++) {
            dealersThreadPool.execute(new GetCar(carStorage));
        }
        controllersThreadPool.execute(new Controller(carStorage, checker));

        Thread.sleep(500);

        accessoriesThreadPool.shutdown();
        motorThreadPool.shutdown();
        bodyThreadPool.shutdown();
        workersThreadPool.shutdown();
        dealersThreadPool.shutdown();
        controllersThreadPool.shutdown();
    }

}
