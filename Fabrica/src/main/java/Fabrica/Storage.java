package Fabrica;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class Storage<T> {
    private final int sSize;
    private final Queue<T> queue;

    public Storage(int sSize){
        this.sSize = sSize;
        queue = new LinkedBlockingQueue<>(sSize);
    }

    public synchronized void put(T obj) {
        while (queue.size() >= sSize)
            try {
                this.wait();
            } catch (InterruptedException e) { e.printStackTrace(); }
        queue.add(obj);

        this.notifyAll();
    }

    public synchronized T get() {
        while (queue.size() < 1)
            try {
                this.wait();
            } catch (InterruptedException e) {e.printStackTrace(); }
        T obj = queue.remove();

        this.notify();
        return obj;
    }

    public synchronized int getSize(){
        return queue.size();
    }

    public synchronized int getsSize(){
        return sSize;
    }

}
