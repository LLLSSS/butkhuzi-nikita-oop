package Fabrica.Contorl;

import Fabrica.Storage;
import Fabrica.Parts.Accessories;
import Fabrica.Parts.Body;
import Fabrica.Parts.Car;
import Fabrica.Parts.Motor;
import Fabrica.Swing.Window;
import java.time.Duration;

public class VisualOperator {
    static public void start(Window visual, TimeSettings timeSettings,
                             final Storage<Motor> engineStorage, final Storage<Body> bodyStorage,
                             final Storage<Car> carStorage, final Storage<Accessories> accessoriesStorage
    ){
        Thread checker = new Thread(() -> {
            while (true){
                try {
                    Thread.sleep(Duration.ofNanos(500).toNanos());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                timeSettings.setACCESSORIES_DEALER_TIME(visual.getACCESSORIES_DEALER_TIME());
                timeSettings.setBODY_DEALER_TIME(visual.getBODY_DEALER_TIME());
                timeSettings.setWORKER_TIME(visual.getWORKER_TIME());
                timeSettings.setMOTOR_DEALER_TIME(visual.getMOTOR_DEALER_TIME());
                timeSettings.setCAR_DEALER_TIME(visual.getCAR_DEALER_TIME());

                visual.setFullnessMotorStorage(engineStorage.getSize(), engineStorage.getsSize());
                visual.setFullnessBodyStorage(bodyStorage.getSize(), bodyStorage.getsSize());
                visual.setFullnessCarStorage(carStorage.getSize(), carStorage.getsSize());
                visual.setFullnessAccessoriesStorage(accessoriesStorage.getSize(), accessoriesStorage.getsSize());

            }
        });
        checker.start();
    }

    public static int getBODY_DEALER_TIME_DEFAULT(){       return TimeSettings.BODY_DEALER_TIME_DEFAULT;}
    public static int getMOTOR_DEALER_TIME_DEFAULT(){     return TimeSettings.MOTOR_DEALER_TIME_DEFAULT;}
    public static int getACCESSORIES_DEALER_TIME_DEFAULT(){return TimeSettings.ACCESSORIES_DEALER_TIME_DEFAULT;}
    public static int getCAR_DEALER_TIME_DEFAULT(){        return TimeSettings.CAR_DEALER_TIME_DEFAULT;}
    public static int getWORKER_TIME_DEFAULT(){            return TimeSettings.WORKER_TIME_DEFAULT;}
}

