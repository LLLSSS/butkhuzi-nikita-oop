package Fabrica.Contorl;

import java.time.Duration;

public class TimeSettings {
    public static int BODY_DEALER_TIME_DEFAULT = 5;
    public static int MOTOR_DEALER_TIME_DEFAULT = 5;
    public static int ACCESSORIES_DEALER_TIME_DEFAULT = 5;
    public static int CAR_DEALER_TIME_DEFAULT = 15;
    public static int WORKER_TIME_DEFAULT = 15;
    public TimeSettings(){
        BODY_DEALER_TIME        = Duration.ofSeconds(BODY_DEALER_TIME_DEFAULT);
        MOTOR_DEALER_TIME      = Duration.ofSeconds(MOTOR_DEALER_TIME_DEFAULT);
        ACCESSORIES_DEALER_TIME = Duration.ofSeconds(ACCESSORIES_DEALER_TIME_DEFAULT);
        CAR_DEALER_TIME         = Duration.ofSeconds(CAR_DEALER_TIME_DEFAULT);
        WORKER_TIME             = Duration.ofSeconds(WORKER_TIME_DEFAULT);
    }
    private Duration BODY_DEALER_TIME;
    private Duration MOTOR_DEALER_TIME;
    private Duration ACCESSORIES_DEALER_TIME;
    private Duration CAR_DEALER_TIME;
    private Duration WORKER_TIME;

    protected void setBODY_DEALER_TIME(Duration BODY_DEALER_TIME){
        this.BODY_DEALER_TIME = BODY_DEALER_TIME;
    }
    protected void setMOTOR_DEALER_TIME(Duration MOTOR_DEALER_TIME){
        this.MOTOR_DEALER_TIME = MOTOR_DEALER_TIME;
    }
    protected void setACCESSORIES_DEALER_TIME(Duration ACCESSORIES_DEALER_TIME){
        this.ACCESSORIES_DEALER_TIME = ACCESSORIES_DEALER_TIME;
    }
    protected void setCAR_DEALER_TIME(Duration CAR_DEALER_TIME){
        this.CAR_DEALER_TIME = CAR_DEALER_TIME;
    }
    protected void setWORKER_TIME(Duration WORKER_TIME){
        this.WORKER_TIME = WORKER_TIME;
    }

    public Duration BODY_DEALER_TIME        (){ return BODY_DEALER_TIME; }
    public Duration MOTOR_DEALER_TIME      (){ return MOTOR_DEALER_TIME; }
    public Duration ACCESSORIES_DEALER_TIME (){ return ACCESSORIES_DEALER_TIME; }
    public Duration CAR_DEALER_TIME         (){ return CAR_DEALER_TIME; }
    public Duration WORKER_TIME             (){ return WORKER_TIME; }
}

